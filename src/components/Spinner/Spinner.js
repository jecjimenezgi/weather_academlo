import "./Spinner.css";

function Spinner(props) {
  if (props.loading) {
    return (
      <div className="container">
        <div className="spin"></div>
      </div>
    );
  } else {
    return <div style={{ display: "none" }}></div>;
  }
}

export default Spinner;
