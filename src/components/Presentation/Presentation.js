import "./Presentation.css";
import { useState, useEffect } from "react";

function Presentation(props) {

    const [temp, setTemp] = useState(0);
    const [tempName, setTempName] = useState("");
    const [tempUnit, setTempUnit] = useState("");
  
    useEffect(() => {
      setTemp(props.celcius);
      setTempName("Celcius");
      setTempUnit("°C");
    }, [props.celcius]);

  if (props.loading){ 
      return (<div style={{display: "none"}}></div>)
  }
  else { 

  
    return (
      <div className="container">
        <div>
          <ul>
            <li>
              <h2>Latitude: {props.lat}</h2>
            </li>
            <li>
              <h2>Longitude: {props.lon}</h2>
            </li>
            <li>
              <h2>City: {props.city}</h2>
            </li>
            <li>
              <h2>Region: {props.region}</h2>
            </li>
            <li>
              <h2>Country: {props.country}</h2>
            </li>
            <li>
              <h2>
                {tempName}: {temp} {tempUnit}{" "}
                <button
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    if (tempName === "Celcius") {
                      setTempName("Fahrenheit");
                      setTemp(props.fahrenheit);
                      setTempUnit("°F");
                    } else {
                      setTempName("Celcius");
                      setTemp(props.celcius);
                      setTempUnit("°C");
                    }
                  }}
                >
                  *
                </button>
              </h2>
            </li>
            <li>
              <h2>Condition: {props.condition} </h2>
            </li>
          </ul>
        </div>
        <div>
          <img src={props.thumbnail} alt="weather condition"></img>
        </div>
      </div>
    );

  }
  

}

export default Presentation;
