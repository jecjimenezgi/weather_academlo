function getWeather(lat,lon, setCity,setRegion ,setCountry,setCelcius, setFahrenheit,setCondition,setThumbnail,setLoading) {
    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
      };
      
      fetch("https://api.weatherapi.com/v1/current.json?key=48a25c7ae3f94b429e9134818210407&q=" + lat + "," + lon +"&aqi=no&e7e8bca91b42002946293dbe10e1969b=appid", requestOptions)
        .then(response => response.json())
        .then(result => {
            setCity(result.location.name)
            setRegion(result.location.region)
            setCountry(result.location.country)
            setCelcius(result.current.temp_c)
            setFahrenheit(result.current.temp_f)
            setCondition(result.current.condition.text)
            setThumbnail(result.current.condition.icon)
            setLoading(false)
    
        })
        .catch(error => console.log('error', error));
}

export default getWeather