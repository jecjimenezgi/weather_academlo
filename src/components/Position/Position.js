function getPosition (setLat,setLon){ 

  
  
        function success(position) {
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;
          setLat(latitude)
          setLon(longitude)

  
        }
  
        function error() {
          alert("Unable to retrieve your location");
        }
  
        if (!navigator.geolocation) {
          alert("Geolocation is not supported by your browser");
        } else {
          console.log("Locating…");
          navigator.geolocation.getCurrentPosition(success, error);
        }
      
}

export default getPosition
