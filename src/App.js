import "./App.css";
import getPosition from "./components/Position/Position";
import getWeather from "./components/Weather/Weather";

import Presentation from "./components/Presentation/Presentation";
import Spinner from "./components/Spinner/Spinner";

import { useState, useEffect } from "react";

function App() {
  const [lat, setLat] = useState(0);
  const [lon, setLon] = useState(0);
  const [city, setCity] = useState("");
  const [region, setRegion] = useState("");
  const [country, setCountry] = useState("");
  const [celcius, setCelcius] = useState(0);
  const [fahrenheit, setFahrenheit] = useState(0);
  const [condition, setCondition] = useState("");
  const [thumbnail, setThumbnail] = useState("");
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    getPosition(setLat, setLon);
  }, []);

  useEffect(() => {
    if (lat && lon) {
      getWeather(
        lat,
        lon,
        setCity,
        setRegion,
        setCountry,
        setCelcius,
        setFahrenheit,
        setCondition,
        setThumbnail,
        setLoading
      );
    }
  }, [lat, lon]);

  return (
    <div>
      <Spinner loading={loading}></Spinner>
      <Presentation loading={loading} lat={lat} lon={lon} city={city} region={region} country={country} celcius={celcius} fahrenheit={fahrenheit} condition={condition} thumbnail={thumbnail} ></Presentation>
    </div>
  );
}

export default App;
